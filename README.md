# Pet Tracker (Registro de Mascotas)

Ésta es una pequeña aplicación para seguir el tutorial rápido del [framework web Amber][amber].

- [Pet Tracker (Registro de Mascotas)](#pet-tracker-registro-de-mascotas)
  - [Uso de la aplicación](#uso-de-la-aplicaci%C3%B3n)
    - [Instalación de shards](#instalaci%C3%B3n-de-shards)
    - [Preparación de la base de datos](#preparaci%C3%B3n-de-la-base-de-datos)
    - [Uso de la aplicación (modo de desarrollo)](#uso-de-la-aplicaci%C3%B3n-modo-de-desarrollo)
  - [Entorno virtual con Vagrant](#entorno-virtual-con-vagrant)
    - [Comandos útiles para el entorno virtual](#comandos-%C3%BAtiles-para-el-entorno-virtual)

## Uso de la aplicación

- Antes que nada, clona el repositorio:

  `git clone https://akaiiro@bitbucket.org/akaiiro_projects/concept-amber-framework-quick-start.git pet-tracker`

Se asume que el equipo que corre la aplicación ya tiene instalado el siguiente stack:

- Compilador de Crystal v0.26.x
- Amber Web Framework v0.9.x
- PostgreSQL v10.x

**Nota**: Si no deseas instalar todas las dependencias sobre tu equipo de desarrollo, recomiendo que uses la máquina virtual definida con éste proyecto. Para ello, realiza primero los pasos definidos
en la sección [«Entorno virtual con Vagrant»](#entorno-virtual-con-vagrant).

### Instalación de shards

- Instalar las dependencias del proyecto:

> `shards install`

### Preparación de la base de datos

**Nota**: Antes de inicializar la base de datos, probablemente sea necesario escribir el usuario y contraseña correctos para tu configuración de Postgres en el campo `database_url` del string de conexión para Postgres detro del archivo `config/environments/development.yml`.

- Preparar la base de datos

> `amber db create migrate`

### Uso de la aplicación (modo de desarrollo)

- Iniciar el servidor:

> `amber watch`

- Abrir el navegador en la página del servidor [http://localhost:3000][localhost].

## Entorno virtual con Vagrant

El entorno virtual instala todas las dependencias dentro de una maquina virtual para no contaminar tu equipo de desarrollo con software que tal vez sólo utilices para éste proyecto. Así, cuando termines de jugar con el proyecto, simplemente puedes destruir la maquina virtual y dejar sólo el código para poder usarlo de nuevo en el futuro.

Para poder usar éste y otros entornos virtuales, necesitas instalar:

- [VirtualBox][vbox] (y su paquete de extensiones).
- [Vagrant][vagrant].

Una vez que tengas VirtualBox y Vagrant instalados, sigue los siguientes pasos:

- Muevete a la carpeta del proyecto

> `cd pet-tracker`

- Inicia la máquina virtual (tardará un poco la primera vez)

> `vagrant up`

- Reinicia la máquina virtual

> `vagrant reload`

- Conectate a la máquina virtual

> `vagrant ssh`

- Continua con las instrucciones de [Instalación de shards](#instalacion-de-shards)

### Comandos útiles para el entorno virtual

- Para cerrar la conexión con la máquina virtual, usa la instrucción

> `exit`

- Para apagar la máquina virtual, usa

> `vagrant halt`.

- Para borar la maquina virtual, usa

> `vagrant destroy`

---
[amber]: https://amberframework.org/
[localhost]: http://localhost:3000/
[vbox]: https://www.virtualbox.org/wiki/Downloads
[vagrant]: https://www.vagrantup.com/downloads.html
